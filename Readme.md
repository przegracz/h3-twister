<!--  ![map](/logo/base.png)) -->

# Map pack schemes

Jebus-Twister (size L)     |  Twister (size M)
:-------------------------:|:-------------------------:
<img src="/schemes/h3-jebus-twister.png"  width="300" height="400"> | <img src="/schemes/h3-twister.png"  width="300" height="400">

# Click [here](https://gitlab.com/przegracz/h3-twister/-/archive/master/h3-twister-master.zip) to download the map packs

# Intro

This is the original repository of Twister mirror map pack for the expansion Horn of the Abyss of the computer game Heroes of Might and Magic III. The above schemes shows the basic structure of each of the map packs. The question mark correspond to the elements that are randomised. This reporistory includes the scripts used to generate these map packs.


# Installation and how to play
To play on this map pack you will need Heroes 3 with [HD mod](https://sites.google.com/site/heroes3hd/) and [HotA](http://download.h3hota.com/HotA_setup).
Just like with any other map, simply [download the archive with the folders containing the map packs](https://gitlab.com/przegracz/h3-twister/-/archive/master/h3-twister-master.zip), unpack them, and move the map pack folders that are in the unpacked [HotA_RMGTemplates folder](HotA_RMGTemplates) to your `HotA_RMGTemplates` folder, inside game's folder on your machine.

# Major features
* The map pack contains many different mirror map templates created procedurally. At the beginning of the game a map is drawn from these map templates.
* The players do not know which exact map template they play to encourage the exploration of the map and different strategies
* Each map is a mirror with several portals to the other side of the mirror:
    * 4 portals (map pack Jebus-Twister)
    * 3 portals (map pack Twister)
    * 2 portals (map pack Mobious)


# Files
* The [generate-map-pack.py](src/generate-map-pack.py) is the script generating the Twister map pack
* The [map packs](HotA_RMGTemplates) that you want to play shall be copied to your `HotA_RMGTemplates` folder

# Detailed description for developers and curious minds

### Jebus-Twister
* The neighbouring zones may be connected with each other and may have wide connection with the starting zone (parameter `contypes`)
* In the starting zone there are 2 netural castles/towns and the may have the same alignment as the starting castle (parameter `leveltowns`)


### Twister
* Each side of a mirror map contains 1 starting zone and 2-3 neighbouring zones, which are connected to the starting zone (parameters `nz1` and `nz2`)
* There are from 2 to 4 castles in each side of the map (parameter `leveltowns`)
* Starting zone may be twice bigger than other zones and contains 1 or 2 towns (parameters `startsize` and `leveltowns`)
* Each neighbouring zone may contain 1 town (parameter `leveltowns`)
* The neighbouring zones may be connected with each other and may have wide connection with the starting zone (parameter `contypes`)
* The strength of monsters may be randomized (parameter `monsters`)
* There are from 0 to 3 gold mines on each side of the map (parameter `extraresources`)



