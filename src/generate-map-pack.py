from __future__ import print_function

import sys, time, os, copy, shutil, math

# directories
mapsdir = "../HotA_RMGTemplates/"
version = "0.64"

def read_headers_protoline( fnm ):
   # read prototype
   with open(fnm, "r") as f:
      text = f.readlines()

      headers = text[0:3]
      # headers[0] = [ e.strip() for e in text[0].split('\t') ]
      # headers[1] = [ e.strip() for e in text[1].split('\t') ]
      # headers[2] = [ e.strip() for e in text[2].split('\t') ]
      # protoline1 = [ e.strip() for e in text[3].split('\t') ]
      # protoline2 = [ e.strip() for e in text[3].split('\t') ]

      headers[0] = [ e for e in text[0].split('\t') ]
      headers[1] = [ e for e in text[1].split('\t') ]
      headers[2] = [ e for e in text[2].split('\t') ]
      protoline1 = [ e for e in text[3].split('\t') ]
      protoline2 = [ e for e in text[4].split('\t') ]

      # pack_cfg = protoline1[0:13]
      # map_cfg = protoline1[13:25]
      # zone_cfg = protoline1[25:108]

   return headers, (protoline1, protoline2)


def initialize_decoders(headers):
   idz = dict()
   for i,e in enumerate(headers[2][0:108]): idz[e.strip()] = i
   # print("Keys:", idz.keys())
   idz["Player minimum towns"] = 36
   idz["Player minimum castles"] = 37
   idz["Player town density"] = 38
   idz["Player castle density"] = 39

   idz["Wood"] = 55
   idz["Mercury"] = 56
   idz["Ore"] = 57
   idz["Sulfur"] = 58
   idz["Crystal"] = 59
   idz["Gems"] = 60
   idz["Gold"] = 61

   idz["Low1"] = 92
   idz["High1"] = 93
   idz["Density1"] = 94
   idz["Low2"] = 95
   idz["High2"] = 96
   idz["Density2"] = 97
   idz["Low3"] = 98
   idz["High3"] = 99
   idz["Density3"] = 100
   return idz


# zone functions:
#
# mobius:
# 1 - central zone, may widely connect with zone 3
# 2 - portal zone, always has portal, may have town, may connect with zone 3
# 3 - potential wide hub zone, very often has town, but not necesarilly a portal
# 4 - optional zone, always has portal, may have town, has no extra connections


def get_zones( mappackname, mapname, headers, protolines, nzones1, nzones2,
   startsize, leveltown, monster, extraresource, contype,
   firstinpack=True ):

   # temporary solution
   idz = initialize_decoders(headers)

   # inits
   zones = list()
   nzones = nzones1+nzones2
   for i in range(1,nzones+1):

      if i==1:
         zone = copy.copy(protolines[0][0:108])
         zone[7] = mappackname.title()+" mirror map pack version "+version
         zone[13] = mapname
         # zone[idz["Strength"]] = monster

         # towns faction
         if "1T" in leveltown or "1F" in leveltown:
            zone[idz["Towns are of same type"]] = "1"
         else:
            zone[idz["Towns are of same type"]] = "0"

         # towns or castles
         if "1ff" in leveltown.lower():
            zone[idz["Minimum towns"]] = ""
            zone[idz["Minimum castles"]] = "2"
         elif "1ft" in leveltown.lower():
            zone[idz["Minimum towns"]] = "1"
            zone[idz["Minimum castles"]] = "1"
         elif "1tt" in leveltown.lower():
            zone[idz["Minimum towns"]] = "2"
            zone[idz["Minimum castles"]] = ""
         elif "1t" in leveltown.lower():
            zone[idz["Minimum towns"]] = "1"
            zone[idz["Minimum castles"]] = ""
         elif "1f" in leveltown.lower():
            zone[idz["Minimum towns"]] = ""
            zone[idz["Minimum castles"]] = "1"
      elif i>1:
         zone = copy.copy(protolines[1][0:108])

         # towns faction
         zone[idz["Player minimum castles"]] = ""
         if "2T" in leveltown or "2F" in leveltown:
            zone[idz["Towns are of same type"]] = "1"
         else:
            zone[idz["Towns are of same type"]] = "0"

         # towns or castles
         if "2t" in leveltown.lower() and i==3:
            zone[idz["Minimum towns"]] = "1"
            zone[idz["Minimum castles"]] = ""
         elif "2f" in leveltown.lower() and i==3:
            zone[idz["Minimum towns"]] = ""
            zone[idz["Minimum castles"]] = "1"

      if "jebus" not in mappackname:

         # starting zone
         if i==1:
            # # init
            # zone = copy.copy(protolines[0][0:108])
            # # print(zone)
            # zone[7] = mappackname.title()+" mirror map pack version "+version
            # zone[13] = mapname

            # basic
            zone[idz["Strength"]] = monster
            zone[idz["Base Size"]] = "10"
            # zone[idz["Base Size"]] = "20"
            if startsize=="2":
               # basic
               zone[idz["Base Size"]] = "20"
               # zone[idz["Base Size"]] = "30"
               # zone[idz["Base Size"]] = "36"

               # towns
               if "1T" in leveltown or "1F" in leveltown:
                  zone[idz["Towns are of same type"]] = "1"
               else:
                  zone[idz["Towns are of same type"]] = "0"
               if "1t" in leveltown.lower():
                  zone[idz["Minimum towns"]] = "1"
                  zone[idz["Minimum castles"]] = ""
               elif "1f" in leveltown.lower():
                  zone[idz["Minimum towns"]] = ""
                  zone[idz["Minimum castles"]] = "1"

               # mines
               zone[idz["Wood"]] = "2"
               zone[idz["Ore"]] = "2"

            if extraresource=="g1": zone[idz["Gold"]] = "1"
            elif extraresource=="g2": zone[idz["Gold"]] = "0"

            # # treasures
            # zone[idz["High1"]] = "15000"
            # zone[idz["Density1"]] = "2"
            # zone[idz["Density2"]] = "6"
            # zone[idz["Density3"]] = "6"

         # other zones
         if i>1:
            # init
            zone = copy.copy(protolines[1][0:108])

            # basic
            zone[idz["Base Size"]] = "10"
            # zone[idz["Base Size"]] = "15"
            # zone[idz["Base Size"]] = "16"
            # zone[idz["Base Size"]] = "20"
            # zone[idz["Base Size"]] = "18"

            zone[idz["human start"]] = ""
            zone[idz["Treasure"]] = "x"
            zone[idz["Ownership"]] = ""

            # towns
            zone[idz["Player minimum castles"]] = ""
            if "2T" in leveltown or "2F" in leveltown:
               zone[idz["Towns are of same type"]] = "1"
            else:
               zone[idz["Towns are of same type"]] = "0"
            if "2t" in leveltown.lower() and i==3:
               zone[idz["Minimum towns"]] = "1"
               zone[idz["Minimum castles"]] = ""
            elif "2f" in leveltown.lower() and i==3:
               zone[idz["Minimum towns"]] = ""
               zone[idz["Minimum castles"]] = "1"
            if "2ft" in leveltown.lower():
               if nzones2==2 and i==2 or nzones2==3 and i==4:
                  zone[idz["Minimum towns"]] = "1"
                  zone[idz["Minimum castles"]] = ""
            elif "2tt" in leveltown.lower():
               if nzones2==2 and i==2 or nzones2==3 and i==4:
                  zone[idz["Minimum towns"]] = "1"
                  zone[idz["Minimum castles"]] = ""

            # mines
            zone[idz["Gems"]] = ""
            zone[idz["Crystal"]] = ""
            zone[idz["Sulfur"]] = ""
            zone[idz["Mercury"]] = ""
            zone[idz["Wood"]] = ""
            zone[idz["Ore"]] = ""
            if "w" in contype and startsize=="1":
               zone[idz["Wood"]] = "1"
               zone[idz["Ore"]] = "1"

            if extraresource=="g1": zone[idz["Gold"]] = "0"
            elif extraresource=="g2": zone[idz["Gold"]] = "1"

            # # treasures
            # zone[idz["High1"]] = "15000"
            # zone[idz["Density1"]] = "2"
            # zone[idz["Density2"]] = "6"
            # zone[idz["Density3"]] = "6"

            # # treasures
            # zone[idz["Low1"]] = "6000"
            # zone[idz["High1"]] = "15000"
            # zone[idz["Density1"]] = "2"
            # zone[idz["Low2"]] = "3000"
            # zone[idz["High2"]] = "6000"
            # zone[idz["Density2"]] = "6"
            # zone[idz["Low3"]] = "300"
            # zone[idz["High3"]] = "3000"
            # zone[idz["Density3"]] = "6"

            # treasures
            zone[idz["Low2"]] = "8999"
            zone[idz["High2"]] = "15000"
            zone[idz["Density2"]] = "3"
            zone[idz["Low3"]] = "6000"
            zone[idz["High3"]] = "8999"
            zone[idz["Density3"]] = "6"
            zone[idz["Low1"]] = "300"
            zone[idz["High1"]] = "6000"
            zone[idz["Density1"]] = "6"


      if i>=2: zone = [""]*25 + zone[25:108]
      if not firstinpack: zone = [""]*13 + zone[13:108]
      zone[idz["Id"]] = str(i)
      zones += [copy.copy(zone)]

   return zones

def get_connections( mapname, headers, protolines, nzones1, nzones2, contype,
   portalguard, zoneguard ):

   # initialize decoders
   idc = dict()
   for i,e in enumerate(headers[2][108:]): idc[e.strip()] = i
   # print("Keys:", idc.keys())

   # connections betwen the cetnral and perifierial zones
   nconnections = nzones1*nzones2
   cons = list()
   widezone=3
   for i in range(nzones1+1, nzones1+nzones2+1):
      con = copy.copy(protolines[0][108:])
      con[idc["Zone 1"]] = "1"
      con[idc["Zone 2"]] = str(i)
      con[idc["Value"]] = zoneguard
      con[idc["Border Guard"]] = ""
      con[idc["Road"]] = "Yes"
      if "w" in contype and i==widezone: con[idc["Wide"]] = "x"
      # elif contype=="f": con[idc["Fictive"]] = "x"
      cons += [con]

   # extra normal links between two perifierial zones
   if "l" in contype:
      con = copy.copy(protolines[0][108:])
      con[idc["Zone 1"]] = "2"
      con[idc["Zone 2"]] = str(widezone)
      con[idc["Value"]] = zoneguard
      con[idc["Border Guard"]] = ""
      con[idc["Road"]] = "Yes"
      cons += [con]

   if "t" in contype and nzones2>=3:
      con = copy.copy(protolines[0][108:])
      con[idc["Zone 1"]] = "2"
      con[idc["Zone 2"]] = "4"
      con[idc["Value"]] = zoneguard
      con[idc["Border Guard"]] = ""
      con[idc["Road"]] = "Yes"
      con[idc["Wide"]] = "x"
      cons += [con]

   if "ll" in contype and nzones2>=3:
      con = copy.copy(protolines[0][108:])
      con[idc["Zone 1"]] = "2"
      con[idc["Zone 2"]] = "4"
      con[idc["Value"]] = zoneguard
      con[idc["Border Guard"]] = ""
      con[idc["Road"]] = "Yes"
      cons += [con]

   # there is a bug on the side of in-game mirror map generation
   # which fixes the available portals among all maps in the map pack
   # because of it we cannot mix maps having different numbers of portals
   if "mobius" in mapname:
      portals = [ "x t4 t6", "x t6 t4" ]
      if nzones2==2: portalzones = [nzones1+1, nzones1+2]
      if nzones2==3: portalzones = [nzones1+1, nzones1+3]
   elif "twister" in mapname or "jebus" in mapname:
      portals = [ "x t4 t5", "x t5 t6", "x t6 t4" ]
      if nzones2==3: portalzones = [nzones1+1, nzones1+2, nzones1+3]

   for i,portalzone in enumerate(portalzones):
      con = copy.copy(protolines[0][108:])
      con[idc["Zone 1"]] = str(portalzone)
      con[idc["Zone 2"]] = "-1"
      con[idc["Value"]] = portalguard
      con[idc["Border Guard"]] = portals[i-1]
      cons += [con]

   # direct links
   con = copy.copy(protolines[0][108:])
   con[idc["Zone 1"]] = "1"
   con[idc["Zone 2"]] = "-1"
   if "jebus" in mapname: con[idc["Value"]] = "100000"
   else: con[idc["Value"]] = "50000"
   con[idc["Border Guard"]] = "x t13 t13"
   cons += [con]

   # con = copy.deepcopy(con)
   # con[idc["Border Guard"]] = "x t8 t8"
   # cons += [con]

   return cons

def write_mappack_to_file( headers, templates, mappackname ):
   # write to a map pack file
   with open("map-pack.txt", "w") as f:

      print( "\t".join(headers[0]), file=f )
      print( "\t".join(headers[1]), file=f )
      print( "\t".join(headers[2]), file=f )

      for template in templates:
         zones, cons = template

         minlines = min([len(zones),len(cons)])
         maxlines = max([len(zones),len(cons)])

         if len(zones)>=maxlines:
            for i,e in enumerate(cons):
               print( "\t".join( zones[i]+cons[i]), file=f )
            while i<len(zones):
               i+=1
               print( "\t".join( zones[i]), file=f )
         else:
            for i,e in enumerate(zones):
               print( "\t".join( zones[i]+cons[i]), file=f )
            while i+1<len(cons):
               i+=1
               print( "\t".join( [""]*108+cons[i]), file=f )

   if not os.path.exists(mapsdir+"/h3-"+mappackname):
      os.makedirs(mapsdir+"/h3-"+mappackname)
   shutil.copyfile( "map-pack.txt", mapsdir+"/h3-"+mappackname+"/rmg.txt" )

def print_map_cfg( headers, protolines ):
   # show headers
   print()
   print("Headers:")
   for i,e in enumerate(headers[0]):
      print( "%3d %25s %15s %15s"%( i,
         headers[2][i],
         headers[1][i],
         headers[0][i] ) )

   print()
   print("Map pack and map config:")
   for i in range(25):
      print( "%30s%30s"%( headers[2][i], protolines[0][i] ))

   # only good if no word wrap
   # print( "".join( "%30s"%e for e in headers[2][0:25] ) )
   # print( "".join( "%30s"%e for e in protoline1[0:25] ) )

def generate_map_pack( mappackname ):

   if mappackname=="mobius":
      headers, protolines = read_headers_protoline( "prototype-twister.txt" )

      nz1 = [1]
      nz2 = [3,2]
      extraresources = [ "g1", "g2" ]
      startsizes = [ "1", "2" ]
      contypes = [ "", "w", "l", "wl" ]
      portalguards = ["6000", "12000"]
      zoneguard = "6000"
      leveltowns = [ "1f", "1f2t", "1t2f", "2f", "2tt", "2ft", "1f2ft", "1t2ft" ]
      monsters = [ "weak", "avg" ]
      monsters = [ "avg" ]

      # # tests
      # nz1 = [1]
      # nz2 = [3]
      # extraresources = [ "g1", "g2" ]
      # portalguard = "6000"
      # startsizes = [ "2" ]
      # contypes = [ "sn" ]
      # portalguards = ["12000"]
      # zoneguard = "6000"
      # leveltowns = [ "2t" ]
      # monsters = [ "avg" ]

   elif mappackname=="twister":
      headers, protolines = read_headers_protoline( "prototype-twister.txt" )

      nz1 = [1]
      nz2 = [3]
      extraresources = [ "g1", "g2" ]
      startsizes = [ "1", "2" ]
      contypes = [ "", "w", "l", "wl", "ll" ]
      portalguards = [ "12000" ]
      zoneguard = "6000"
      leveltowns = [ "1f", "1f2t", "1t2f", "2f", "2tt", "2ft", "1f2ft", "1t2ft" ]
      monsters = [ "avg" ]

   elif mappackname=="twister-debug":
      headers, protolines = read_headers_protoline( "prototype-twister.txt" )

      nz1 = [1]
      nz2 = [3]
      extraresources = [ "g2" ]
      startsizes = [ "2" ]
      contypes = [ "ll" ]
      portalguards = [ "12000" ]
      zoneguard = "6000"
      leveltowns = [ "1f" ]
      monsters = [ "avg" ]

   elif mappackname=="jebus-twister":
      headers, protolines = read_headers_protoline( "prototype-jebus-z13.txt" )

      nz1 = [1]
      nz2 = [3]
      extraresources = [ "" ]
      startsizes = [ "" ]
      contypes = [ "w", "l", "t", "wl", "ll", "lt" ]
      portalguards = [ "35000" ]
      zoneguard = "35000"
      leveltowns = [ "1ff", "1FF", "1TT", "1FT" ]
      monsters = [ "avg" ]

   # print_map_cfg( headers, protolines )

   # generate templates
   templates = []
   firstinpack = True
   for nzones2 in nz2:
      for nzones1 in nz1:
         for startsize in startsizes:
            for leveltown in leveltowns:
               for monster in monsters:
                  for contype in contypes:
                     for portalguard in portalguards:
                        for extraresource in extraresources:
                           # ensure a large starting zone if second town present there
                           # ensure at least one neutral city per map
                           if "1" in startsize and "1" in leveltown: continue

                           # ensure a fairly big starting zone
                           if "1" in startsize and "w" not in contype: continue

                           mapname = "h3-%s-z%d%d-s%s-t%s-c%s-m%s-$%s" %(
                              mappackname,
                              nzones1, nzones2, startsize, leveltown,
                              contype, monster[0], extraresource )
                           # mapname = "h3-"+mappackname

                           zones = get_zones( mappackname, mapname, headers,
                              protolines,
                              nzones1, nzones2, startsize, leveltown, monster,
                              extraresource, contype, firstinpack )
                           cons = get_connections( mapname,
                              headers, protolines,
                              nzones1, nzones2, contype,
                              portalguard, zoneguard )
                           templates += [(zones,cons)]
                           firstinpack = False


   templates[0][0][0][8] = """Consists of %d random map templates.
   Rules:
      * double build allowed
      * recommended simultanious turns until 127 or less
      * recommended chess timer: 8min, 4 min, 3 min
      """ % len(templates)

   # templates[0][0][0][8] = ""

   write_mappack_to_file( headers, templates, mappackname )
   print("Saved map pack %s with %d templates"%( mappackname, len(templates) ) )

def main():
   # generate_map_pack( "mobius" )
   # generate_map_pack( "twister" )
   # generate_map_pack( "twister-debug" )
   generate_map_pack( "jebus-twister" )

if __name__ == '__main__':
   main()


